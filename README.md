#Documentation

##Brief

This process gives the posibility of interpreting qr codes. It is obtained from the frame captured by the camera, this interpretation is made by the zbar library.

##Usage

Read from the topic ```/droneX/qr_interpretation``` the information of the qr readed, if it is not a qr in the frame captured or is not correctly read, no message will be sent.

It recieves the camera information from the topic ```/droneX/camera/front/image_raw```, and zbar decodes the image.
