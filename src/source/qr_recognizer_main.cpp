#include "../include/qr_recognizer.h"

int main(int argc, char** argv){
  ros::init(argc, argv, ros::this_node::getName());

  std::cout << ros::this_node::getName() << std::endl;

  QrRecognizer Recognizer;
  Recognizer.setUp();
  ros::Rate rate(10);
  while(ros::ok()){
    ros::spinOnce();
    Recognizer.run();
    rate.sleep();
  }
  return 0;
}
