#include "../include/qr_recognizer.h"

QrRecognizer::QrRecognizer(): DroneProcess()
{
}
QrRecognizer::~QrRecognizer()
{
}

void QrRecognizer::ownSetUp()
{
  n.param<std::string>("drone_id", drone_id,"1");

  n.param<std::string>("qr_interpretation_topic", notification_topic,  "qr_interpretation");

  n.param<std::string>("drone_console_interface_sensor_front_camera", drone_console_interface_sensor_front_camera,"camera/front/image_raw");
}

void QrRecognizer::ownStart()
{
  //Subscribers
  image_front_sub_ = n.subscribe(drone_console_interface_sensor_front_camera, 1, &QrRecognizer::imagesFrontReceptionCallback, this);

  //Publishers
  notification_pub = n.advertise<droneMsgsROS::QRInterpretation>(notification_topic, 1, true);

  scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);
}

void QrRecognizer::ownStop()
{
  image_front_sub_.shutdown();
}

void QrRecognizer::ownRun()
{
    if(cv_front_image)
    {


      // Check for invalid input
      int width = cv_front_image->image.cols;
      int height = cv_front_image->image.rows;
      uchar *raw = (uchar *)cv_front_image->image.data;
      Image image(width, height, "Y800", raw, width * height);
      // scan the image for barcodes
      int n = scanner.scan(image);

      Image::SymbolIterator symbol = image.symbol_begin();

      entered=false;
      // extract results
      for(Image::SymbolIterator symbol = image.symbol_begin();
        symbol != image.symbol_end();
        ++symbol) {
                   entered=true;
                   std::vector<cv::Point> vp;
        // do something useful with results
        droneMsgsROS::QRInterpretation interpretation;
        interpretation.message= symbol->get_data();
        for(int i = 0; i < interpretation.message.length(); i++)
        {
          if(interpretation.message[i] == ' ')
          interpretation.message[i] = '_';
         }
        notification_pub.publish(interpretation);
      }
      if(!entered){
      droneMsgsROS::QRInterpretation interpretation;
      interpretation.message="";
      notification_pub.publish(interpretation);}

      // clean up
    }

}

void QrRecognizer::imagesFrontReceptionCallback(const sensor_msgs::ImageConstPtr& msg){
  mtx.lock();
  try
  {
    cv_front_image = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  // ToDo := If mutex implementation doesn't unlock upon destruction or destruction doesn't happen
  // when going through the catch clause, there will be a deadlock
  mtx.unlock();
}
