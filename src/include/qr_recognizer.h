#include <cstddef>

#include "cv.h"
#include "highgui.h"
#include <iostream>
#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <droneMsgsROS/QRInterpretation.h>
#include "communication_definition.h"
#include <drone_process.h>
#include <thread>
#include <mutex>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//#include "opencv2/imgcodecs.hpp"
#include <stdio.h>
#include <math.h>
#include "zbar.h"

using namespace zbar;


class QrRecognizer: public DroneProcess
{

  public:
    QrRecognizer();
    ~QrRecognizer();

  private:
      void ownSetUp();
      void ownRun();
      void ownStop();
      void ownStart();

  private:
    bool subscriptions_complete;

    std::string drone_id;
    std::string notification_topic;
    std::string drone_console_interface_sensor_front_camera;
    std::mutex mtx;


    ros::Publisher notification_pub;
    ros::Subscriber image_front_sub_;


  public:
    ros::NodeHandle n;
    unsigned int cols, rows, neighborhood_vertical, neighborhood_horizontal, x, y, i, j, max_intensity;
    cv::Mat img, gray_img;
    cv::Scalar mean_value, stddev;
    float mean_val;
    bool entered;
    int thresh = 100;
    ImageScanner scanner;
    cv_bridge::CvImagePtr cv_front_image;

    const char* connected_elements = "Elements in the image";

    void imagesFrontReceptionCallback(const sensor_msgs::ImageConstPtr& msg);

};
